<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="img/bl.png">

  <!--Boostrap-css-->
  <link rel="stylesheet" href="css/animation-aos.css">
  <link href="css/aos.css" rel="stylesheet prefetch" type="text/css" media="all">
  <!-- Animation-->
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
  <!--Style css-->
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/main.css" type="text/css" media="all">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <title>Beacon Link</title>
  <!-- Font Awesome Icons-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Google Fonts-->
  <link rel="dns-prefetch" href="//www.google.com">
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS-->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap-->
  <link href="scss/navbar.scss" rel="stylesheet">
 
  <!--Video modal-->

  <!--Carousel-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/scripts.js"></script>		

</head>

<body id="page-top">

  <!--Preloader-->
  <div class="preloader">
    <div class="inner">
        <div class="item item1"></div>
        <div class="item item2"></div>
        <div class="item item3"></div>
    </div>
</div>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg  fixed-top py-3" id="mainNav" style="color:white; background:black">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"> <i class="fas fa-bars" style="color:#fff; font-size:28px;"></i></span>
  </button>
  <a class="navbar-brand"><img src="" class="logo"></a>
  <div class="col-md-2"></div>
  <div class="col-md-8 collapse navbar-collapse text-center" id="navbarTogglerDemo02" style="">
  <ul class="navbar-nav mr-auto mt-2 mt-md-0 text-center">
      <li class="nav-item active">
        <a class="nav-link js-scroll-trigger"  href="#page-top">Home</a>
      </li>
      <li class="nav-item">
          <a class="nav-link js-scroll-trigger text-center"  href="#aboutus">About us</a>
      </li>
      <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#goverment">Government</a>
      </li>
      <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#training">Training</a>
      </li>
      <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#services">Services</a>
      </li>
      <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#client">Clients</a>
      </li>
      <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="contact.php">Contact us</a>
      </li>
      <li class="nav-item">
      <a class="nav-link js-scroll-trigger" target="_blank" href="https://hermes.beacon-link.com/login">Sign In</a>
      </li>
    </ul>
  </div>
  <div class="col-md-2"></div>
   
</nav>
  <!-- Masthead -->

<header class="cabecera" style="background:#000">
 <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold"><img src="img/logo.png" width="500" height="600"></h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">Communicational Solutions in 200+ languages</p>
          
        </div>
      </div>
    </div>
</header>

  <!-- About Section -->


<!--Section About-->

<section>
    <div class="main-w3pvt">
      <div class="container-fluid">
        <div class="row">
          <div class="container py-xl-5 py-lg-3">
            <!-- About us Section -->
            <section  id="aboutus" style="padding-top:300px;">
                <div class="page-section mt-0 col-md-12 row ">
                    <!-- Lado Izquierdo -->
                    <div class="izq col-md-6 text-justify mt-4 ">
                      <div style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">
                        <h1 class="text-justify text-white text-center">About us</h1>
                        <p class="text-justify text-white">Beacon Link manages mechanisms of intercultural communication, providing educational, marketing and media solutions to Individuals, Corporate and Government clients at a Local, Nationwide and International scale facilitated by certified professionals delivering the power of language and media to promote expansion and worldwide connections.</p>
                        <p class="text-justify text-white">This company is a beacon for language, cultural, and a commercial link between diverse parties to enhance global development.</p>
                        <p class="text-justify text-white" >Beacon Link is a national and international reference for marketing, education, interpretation and translation services; and we generate fidelity with our clients through branding and marketing structures.</p>
                      </div>
                    </div>
                    <!-- Lado Derecho -->
                    <div class="der col-lg-6 col-md-6 mt-4  container">
                      <h1 class="text-justify text-white text-center">Our Team</h1>
                      <img src="img/alfredo-herrera.png" alt="" width=" 330px" height="200px">
                      <br>
                      <br>
                      <h4 class="text-justify text-white text-center">José Alfredo Herrera</h4>
                      <p class="text-justify text-white-75 text-center">CEO</p>
                      <p class="text-justify text-white">
                        Over 20 years of experience in the Interpretation and Translation Field. 
                        Licensed and Principal Instructor of “The Beacon Interpreter Training”</p>
                    </div>
                </div>
            </section>
            </div>          
       
      </div>
    </div>
</section>
  
<!-- fin About-->

<section id="goverment" class="section-bg">
      <div class="container">

        <div class="section-header">
          <h3>CAPABILITY STATEMENT</h3>
          
        </div>

        <div class="row">
          <div class="col-md-12 col-lg-12 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box" style="background-image: linear-gradient(to right, #c9851e , #faa525);">
            <p class="description">Beacon link manages mechanisms of intercultural communication, providing language services, education, marketing and media solutions to individuals, corporate and government clients at nationwide scale facilitated by certified
            professionals delivering the power of language and media to promote expansion and worldwide connections.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box"  style="background-image: linear-gradient(to right, #50b36b , #6cf08f); color:#ffffff;">
              <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
              
              <h4 class="title text-center text-justify">DUNS # 080460898</h4>
              <h4 class="title text-center text-justify">CAGE # 7RYT7</h4>
              <h4 class="title text-center text-justify">M/FBE Certified</h4>
            
          
              
            </div>
            <div class="box box-p"  style="background-image: linear-gradient(to right, #bfbd56 , #e0de63);">
              <div class="icon"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              
              <h4 class="title text-center text-justify">NAICS CODES</h4>
              <ul style="list-style-type: circle">
                          <li>
                          <p><b>541930</b></p>
                          <p>Translation and Interpretation Services</p>
                          </li>
                          <li>
                          <p><b>541990</b></p>
                          <p>All Other Professional Scientific and Technical Services</p>
                          </li>
                          <li>
                          <p><b>611710</b></p>
                          <p>Educational Support Services</p>
                          </li>
                          <li>
                          <p><b>541611</b></p>
                          <p>Administrative Mgmt and General Mgmt Consulting Services</p>
                          </li>
                          <li>
                          <p><b>541618</b></p>
                          <p>Other Mgmt Consulting</p>
                          </li>
                          <li>
                          <p><b>561320</b></p>
                          <p>Temporary Help Services</p>
                          </li>
                          <li>
                          <p><b>561499</b></p>
                          <p>All other business support services</p>
                          </li>
                          <li>
                          <p><b>561410</b></p>
                          <p>Document Preparation Services</p>
                          </li>
                          <li>
                          <p><b>561439</b></p>
                          <p>All other business service centers</p>
                          </li>
                        </ul>

                        <br>
              
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box" style="background-image: linear-gradient(to right, #c9851e , #faa525);">
                 
                <h4 class="title text-center text-justify">CURRENT CUSTOMERS</h4>
                <p class="description">Forsyth County Georgia, Cherokee School District, Cartersville City Schools, Perrotta, Lamb & Johnson, Law Firm. Gwinnett Public Schools</p>
                
            
            </div>

            <div class="box" style="background-image: linear-gradient(to right, #1c91c7 , #21b9ff);">
            <div class="icon"><i class="ion-ios-bookmarks-outline" style="color: #e9bf06;"></i></div>
              
                <h5 class="title text-center text-justify">CORE COMPETENCIES</h4>
                <br>
                <h5 class="title2">INTERPRETATION</h5>
                <ul>
                  <li>On-Site</li>
                  <li>In-House</li>
                  <li>Video Remote Interpretation (VRI)</li>
                  <li>Telephonic (OPI)</li>
                </ul>
                
                <h5 class="title2">DOCUMENT TRANSLATION</h5>
                <h5 class="title2">VOICE OVER</h5>
                <h5 class="title2">CLOSE CAPTURE</h5>
                <h5 class="title2">40 HR. INTERPRETER TRAINING</h5>
                
                <br>
                <p class="text-center text-justify"><b>OVER 200 LANGUAGES</b></p>
                <p class="text-center text-justify">(Including American Sign Language)</p>
                <br>
                         
              </div>
            
            <div class="box" style="background-image: linear-gradient(to right, #8847ad , #c55eff);">
              <div class="icon"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              <h4 class="title text-center text-justify">Contact</h4>
              <div class="text-justify text-center">
        
                       
                                    <p class="description">Alfredo Herrera, CEO</p>
                                    <p class="description">685 River Overlook Dr</p>
                                    <p class="description">Lawrenceville, GA 30043</p>
                                 
                               
                                    <p class="description">( 4 7 0 ) 3 1 5 - 4 9 4 9</p>
                                    <p class="description" >AHERRERA@BEACON-LINK.COM</p>
                                    <p class="description"></p>
  
                         
            </div>
          </div>


         

        </div>

      </div>
    </section>


<!--Section Government-->


<!-- end Section Government-->

<!-- Video de presentacion-->

  <div class="video-promo" style="text-align: center;"> 
    <br><br>
    <button class="btn btn-link btn-lg video" data-video="https://www.youtube.com/embed/gZwRwWx91w4?rel=0&amp;wmode=transparent" data-toggle="modal" data-target="#videoModal"><img src="img/play-button.png"></button>
      <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <iframe width="150%" height="350" src="" frameborder="0" requestfullscreen></iframe>
                </div>
            </div>
        </div>
  </div>
  </div>

<!-- Fin about us-->

 <!--Texto de Training-->       
 <div id="training" class="introducing py-5">
    <h1 class="left-text" data-sr-id="5" style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">INTRODUCING</h1>
    <h2 class="left-right" data-sr-id="6" style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">The Beacon Interpreter Training</h2>
  </div>

 

 <!-- Training carousel-->
 
    <div class="training" id="training">
      <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul>
      
        <!-- The slideshow -->
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img style="height: 550px;" src="img/c1.jpeg">
            <div class="carousel-caption"> 
              <h1 style="text-align: left; color: yellow;">Who should attend</h1>
        <ul style="text-align: left; font-size: 25px;">
           <li>
             Bilingual staff experienced in interpreting for clients
          </li>
           <li>
             Individuals interested in becoming Professional Interpreters
          </li>
           <li>
             Individual interpreters who have never taken interpretation training
          </li>
           <li>
             Interpreters interested in becoming	certified
          </li>
        </ul>
        &nbsp;
        <p style="font-size: 25px; text-align: center;"><a class="btn btn-primary btn-warning" href="training.php" tabindex="-1" style="color:black;">ENROLL NOW!</a></p>
            </div>
          </div>
          <div class="slideInd slick-slide" style="background-image: url(&quot;img/Banner-1.png&quot;); width: 424px;" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="0"></div>



          <div class="carousel-item">
            <img style="height: 550px;" src="img/c2.jpeg">
            <div class="carousel-caption"> 
              <h2 style="text-align: center; color: yellow;"><strong>TRAININGS COMING UP</strong></h2>
        <h2 style="text-align: center;">Acworth, GA -&nbsp; Aug 22,23 - Aug 29,30</h2>
        <h2 style="text-align: center;">And - Sep 5th,&nbsp; 2020</h2>
        &nbsp;
        <p style="font-size: 25px; "><a class="btn btn-primary btn-warning" href="training.php" tabindex="-1" style="color:black;">ENROLL NOW!</a></p>
        
        </div>
          </div>




          <div class="carousel-item">
            <img style="height: 550px;" src="img/c3.jpeg">

            <div class="carousel-caption" style="font-size: 15px; text-align: left;"> 
              <h1 style="color: yellow; ">COURSE TOPICS</h1>
        <ul>
          <li>Introduction to Community Interpreting</li>
          <li>Interpretation Protocols</li>
          <li>CLAS Standards, Title VI and HIPPA</li>
          <li>Strategic Mediation</li>
          <li>Professional Identity</li>
          <li>The Role of the interpreter</li>
          <li>Interpreter Role Play with Language Coach</li>
          <li>Strategies for Communicating
        and Connecting Across Cultures</li>
          <li>How to Work with a Professionally
        Trained Interpreter and Untrained Bilingual Staff</li>
        </ul>
        &nbsp;
        <p class="button" style="font-size: 25px; text-align: center;"><a class="btn btn-primary btn-warning" href="training.php" tabindex="-1" style="color:black;">ENROLL NOW!</a></p>
            </div>
          </div>
        </div>
      
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      
      </div>
      



 <!-- Services Section -->
 <section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">Our Services</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-2 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-home text-primary mb-4" style="color: #339af0;"></i>
            <h4 class="h5 mb-2">IN HOUSE INTERPRETERS</h3>
            <p class="text-muted mb-0">All Languages Nationwide</p>
          </div>
        </div>
        <div class="col-lg-2 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-users text-primary mb-4"></i>
            <h4 class="h5 mb-2">On-SITE INTERPRETATION</h3>
            <p class="text-muted mb-0">All Languages Nationwide</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center p-2">
          <div class="mt-5">
            <i class="fas fa-4x fa-headset text-primary mb-4"></i>
            <h4 class="h5 mb-2">VIDEO REMOTE INTERPRETATION(VRI)</h3>
            <p class="text-muted mb-0">Spanish 24/7, all languages including ASL pre-scheduled.</p>
          </div>
        </div>
        <div class="col-lg-2 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-file-alt text-primary mb-4" style="color: #339af0;"></i>
            <h4 class="h5 mb-2">DOCUMENT TRANSLATION</h3>
            <p class="text-muted mb-0">All Languages</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-file-alt text-primary mb-4" style="color: #339af0;"></i>
            <h4 class="h5 mb-2">TELEPHONIC INTERPRETATION(OPI)</h3>
            <p class="text-muted mb-0">24/7 All Languages or Pre-scheduled</p>
          </div>
        </div>
      </div>
    </div>
  </section>



  <!-- Modal goverment-->

      <div class="container">
        
        <!-- The Modal -->
        <div class="modal fade" id="myModal">
          <div class="modal-dialog modal-dialog modal-lg">
            <div class="modal-content">
            
              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title">Government</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              
              <!-- Modal body -->
              <div class="modal-body">
                <object class="pdfdoc" width="750px" height="450px" type="application/pdf" data="info/document.pdf"></object>
              </div>
              
              <!-- Modal footer -->
              
              
            </div>
          </div>
        </div>
        
      </div>

         <!-- Portfolio Section -->
  		<!-- contact -->


      <div class="main-w3pvt">
      <div class="container-fluid">


      <div class="client py-5" id="client" >
        <div class="container py-xl-5 py-lg-3">
          <h3 class=" text-center text-white font-weight">BUSINESSES TRUST BEACON LINK</h3>
          <div class="row inner-sec-w3layouts-w3pvt-lauinfo">
                  
                  <div class="col-sm-3 team-grids text-center">
                    <img src="img/client3.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>CARTERSVILLE MEDICAL</h4>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-3 team-grids text-center">
                    <img src="img/client2.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>CARTERSVILLE CITY SCHOOLS</h4>
                      </div>
                    </div>
                  </div>
                
                  <div class="col-md-3 team-grids text-center">
                    <img src="img/cherokee-county-school.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>CHEROKEE COUNTY SCHOOL</h4>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-3 team-grids text-center">
                    <img src="img/gordon-county-school.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>GORDON COUNTY SCHOOL</h4>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-3 team-grids text-center">
                    <img src="img/perrottalaw_logo.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>PERROTTA, LAMB &#038; JOHNSON, LLC.</h4>
                      </div>
                    </div>
                  </div>
                    
                  <div class="col-md-3 team-grids text-center">
                    <img src="img/client4.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>BARTOW COUNTY JUVENILE COURT</h4>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-3 team-grids text-center">
                    <img src="img/Forsyth.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>FORSYTH COUNTY, GA</h4>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 team-grids text-center">
                    <img src="img/DCOC.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>DEKALB CHAMBER OF COMMERCE</h4>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 team-grids text-center">
                    <img src="img/HCOC.jpg" class="img-fluid" alt="" />
                    <div class="team-info">
                      <div class="caption">
                        <h4>GEORGIA HISPANIC CHAMBER OF COMMERCE</h4>
                      </div>
                    </div>
                  </div>

              
            </div>         
        </div>
      </div>

      </div>
      </div>
      

  <!-- Footer -->
  <footer class="footer py-5">
		
    <div class="container py-xl-4 py-lg-3">
       <div class="address row mb-4">
            <div class="col-lg-4 address-grid">
                <div class="row address-info">
                    <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                      <i class="fa fa-envelope"></i>
                    </div>
                    <div class="col-md-9 col-8 address-right">
                      <p>
                        <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
                      </p>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 address-grid my-lg-0 my-4">
              <div class="row address-info">
                <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                  <i class="fa fa-phone"></i>
                </div>
                <div class="col-md-9 col-8 address-right">
                  <p class="text-light">Language Department<br>
                  (470) 315-4949 EXT 301<br>
                  Media Department <br>
                  (470) 315-4949 EXT 302<br>
                  Toll-Free<br>
                  (844) 706-7388</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 address-grid">
                <div class="row address-info">
                    <div class="col-md-2 col-3 address-left text-lg-center text-sm-right text-center">
                      <i class="fa fa-map"></i>
                    </div>
                    <div class="col-md-10 col-9 address-right">
                      <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- social icons footer -->
        <div class="w3l-footer text-center pt-lg-4 mt-5">
          <ul class="list-unstyled">
            <li>
              <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
                <img src="img/fb.png" alt="">
              </a>
            </li>
            <li class="mx-1">
              <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
                <img src="img/ig.png" alt="">
              </a>
            </li>
            <li class="mx-1">
              <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
                <img src="img/you.png" alt="">
              </a>
            </li>
          </ul>
        </div>
        <!-- copyright -->
        <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
        <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
        <!-- //copyright -->
      </div>
    
    
  
</footer>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

  <!-- Plugin JavaScript -->
 
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->


	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	
</body>

</html>
