<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Boostrap-css-->
  <link rel="stylesheet" href="css/animation-aos.css">
  <link href="css/aos.css" rel="stylesheet prefetch" type="text/css" media="all">
  <!-- Animation-->
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
  <!--Style css-->
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/main.css" type="text/css" media="all">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <title>Beacon Link</title>
  <!-- Font Awesome Icons-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Google Fonts-->
  <link rel="dns-prefetch" href="//www.google.com">
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS-->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap-->
  <link href="scss/navbar.scss" rel="stylesheet">
  <link href="css/creative.min.css" rel="stylesheet"> 
  <link href="cart/assets/sticky-footer-navbar.css" rel="stylesheet">
  <link href="cart/assets/cart.css" rel="stylesheet">


  <!--Video modal-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <script src="js/scripts.js"></script>

  <!--Carousel-->
  <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://js.stripe.com/v3/"></script>
      </head>
      <body>
      <!-- Navigation-->
      <nav  class="navbar navbar-expand-lg navbar-light fixed-top py-3" style="background-image: linear-gradient(to right bottom, #0f5e92, #23acf3);">
        <div class="container">
        <a class="navbar-brand"  href="index.php"><img src="img/logo.png" class="logo"></a>
        
        </div>
	    </nav>

      <!-- Navigation -->
      
      <!--<section class="page-section">-->
      <div class="main-w3pvt" style="padding-top:0px !import;">
          <div class="container-fluid">
            <div class="row">
              <div class="container" style="padding-bottom:150px;">
                <div class="col-md-12">
                  <div>
                    <br><h1 class="h2 text-center text-white font-weight-bold">Thank you for reaching out to us. In order to better help you, tell us what services you are looking for?</h1>
                    </div>
                    <p style="color: white;" class="sub-tittle text-center mt-3 mb-sm-5 mb-4">Select one to send us a service request or to obtain information about the service.</p>
                  </div>
                  <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin: 0;" >
                    <li class="nav-item" style="background-color: white; color: white;">
                      <a class="nav-link" id="one-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="one" aria-selected="false"><span class="fa fab fa-file-alt"></span><span>Document Translation</span></a>
                    </li>
                    <li class="nav-item" style="background-color: white; color: white;">
                      <a class="nav-link" id="two-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="two" aria-selected="false"><span class="fa fab fa-users"></span><span>On-Site Interpretation</span></a>
                    </li>
                    <li class="nav-item" style="background-color: white; color: white;">
                      <a class="nav-link" id="three-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="three" aria-selected="false"><span class="fa fa-headset"></span><span>Telephonic Interpretation</span></a>
                    </li>
                    <li class="nav-item" style="background-color: white; color: white;">
                    <a class="nav-link" id="four-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="four" aria-selected="false"><span class="fa fa-video"></span><span>Video Remote</span></a>
                    </li>
                    <li class="nav-item" style="background-color: white; color: white;">
                      <a class="nav-link" id="five-tab" data-toggle="tab" href="#tab5" role="tab" aria-controls="five" aria-selected="false"><span class="fas fa-calendar-check"></span><span>Interpreter Training</span></a>
                    </li>
                    <li class="nav-item" style="background-color: white; color: white;">
                      <a class="nav-link" id="six-tab" data-toggle="tab" href="#tab6" role="tab" aria-controls="six" aria-selected="false"><span class="fas fa-handshake"></span><span>Be part of Our Network</span></a>
                    </li>
                  </ul>

                <div class="tab-content" id="myTabContent">

                  <div class="tab-pane fade" id="tab1" role="tabpanel" aria-labelledby="one-tab">
                    <div class="form" style="text-align: center">
                      <br>
                      <h2 style="color: white;">Document Translation</h2><br>
                      <br>
                      
                      <?php require_once("contact/section1.php") ?>
                      
                    </div>
                  </div>
      
                  <!-- start section 2 -->
                  <?php require_once("contact/section2.php") ?>
                  <!-- end section 2-->
                  <!-- start section 3 -->
                  <?php require_once("contact/section3.php") ?>
                  <!-- end section 3-->
                  <!-- start section 4 -->
                  <?php require_once("contact/section4.php") ?>
                  <!-- end section 4 -->
                  <!-- start section 5 -->
                  <?php require_once("contact/section5.php") ?>
                  <!-- end section 5 -->


                </div>
              </div>
              <!-- start section 6 -->
              <?php require_once("contact/section6.php") ?>
              <!-- end section 6 -->
                    

          </div>
      </div>
    </div>
</div>
 <!-- Footer -->
  <footer class="footer py-5" style="margin-top:6rem;">
		
      <div class="container py-xl-4 py-lg-3">
        <div class="address row mb-4">
          <div class="col-lg-4 address-grid">
          <div class="row address-info">
            <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
            <i class="fa fa-envelope"></i>
          </div>
          <div class="col-md-9 col-8 address-right">
            <p>
              <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
            </p>
          </div>

        </div>
      </div>
            <div class="col-lg-4 address-grid my-lg-0 my-4">
              <div class="row address-info">
                <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                  <i class="fa fa-phone"></i>
                </div>
                <div class="col-md-9 col-8 address-right">
                  <p class="text-light">Language Department<br>(470) 315-4949 EXT 301<br>
                  Media Department <br>(470) 315-4949 EXT 302<br>Toll-Free<br>
                  (844) 706-7388</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 address-grid">
            <div class="row address-info">
            <div class="col-md-2 col-3 address-left text-lg-center text-sm-right text-center">
                      <i class="fa fa-map"></i>
                    </div>
                    <div class="col-md-10 col-9 address-right">
                      <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
                    </div>
            </div>
          </div>
        </div>
        <!-- social icons footer -->
        <div class="w3l-footer text-center pt-lg-4 mt-5">
          <ul class="list-unstyled">
            <li>
              <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
                <img src="img/fb.png" alt="">
              </a>
            </li>
            <li class="mx-1">
              <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
                <img src="img/ig.png" alt="">
              </a>
            </li>
            <li class="mx-1">
              <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
                <img src="img/you.png" alt="">
              </a>
            </li>
          </ul>
        </div>
  <!-- copyright -->
  <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
  <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
  <!-- //copyright -->
</div>
    
</footer>
<!-- validador.js -->
<script src="js/validador.js" type="text/javascript"></script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

<script>

document.getElementById('Fecha').valueAsDate = new Date();


</script>

<!--calendar -->
<script src="jquery.ui.datepicker-es.js"></script>
<script>
  $(function () {
  $.datepicker.setDefaults($.datepicker.regional["es"]);
  $("#datepicker").datepicker({
  firstDay: 1
  });
  });
</script>

</body>

</html>

    