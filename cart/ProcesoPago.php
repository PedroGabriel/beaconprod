<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Beacon Link</title>
    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/sticky-footer-navbar.css" rel="stylesheet">
    <link href="assets/cart.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="https://js.stripe.com/v3/"></script>
</head>
<body>
    <!-- Navigation-->
    <nav  class="navbar navbar-expand-lg navbar-light fixed-top py-3" style="background-image: linear-gradient(to right bottom, #0f5e92, #23acf3);">
        <div class="container">
        <a class="navbar-brand"  href="../index.php"><img src="image/logo.png" class="logo"></a>
        
        </div>
	  </nav>


<!-- Begin page content -->

<div class="container">
  <h3 class="mt-5 text-center text-justify">Checkout</h3>
  <hr>
  <div class="row">
    <div class="col-12 col-md-12"> 
      <!-- Contenido -->
<?php

$item_quantity = 1;
$item_price = 650;

?>
<div id="shopping-cart">
        <div class="txt-heading">
            <div class="txt-heading-label">Cart</div>

            <a id="btnEmpty" href="index.php?action=empty"><img
                src="image/empty-cart.png" alt="empty-cart"
                title="Empty Cart" class="float-right" /></a>
            <div class="cart-status">
                <div>Total Quantity: <?php echo $item_quantity; ?></div>
                <div>Total Price: $ <?php echo $item_price; ?></div>
            </div>
        </div>
      
                <?php
                require_once ("ListaCarrito.php");
                ?>

      
     

</div>
  <form action="CreateCharge.php" method="post" id="payment-form">  
        <div class="frm-heading">
            <div class="txt-heading-label">BILLING DETAILS</div>
        </div>
        <div class="m-4" id="card-element" >
          <!-- A Stripe Element will be inserted here. -->
        </div>

        <!-- Used to display form errors. -->
        <div id="card-errors" role="alert"></div>
        <div>
          <input type="submit" class="btn-action" name="proceed_payment" value="Place Order">
        </div>
    </form>
      <!-- Fin Contenido --> 
    </div>
  </div>
  <!-- Fin row --> 

  
</div>
<!-- Fin container -->
<footer class="footer py-5">
		
        <div class="container py-xl-4 py-lg-3">
           <div class="address row mb-4">
              <div class="col-lg-4 address-grid">
                 <div class="row address-info">
                  <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <div class="col-md-9 col-8 address-right">
                    <p>
                      <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
                    </p>
                  </div>
    
                </div>
              </div>
              <div class="col-lg-4 address-grid my-lg-0 my-4">
                <div class="row address-info">
                  <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                    <i class="fa fa-phone"></i>
                  </div>
                  <div class="col-md-9 col-8 address-right">
                    <p class="text-light">Language Department<br>
                    (470) 315-4949 EXT 301<br>
                    Media Department <br>
                    (470) 315-4949 EXT 302<br>
                    Toll-Free<br>
                    (844) 706-7388</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 address-grid">
                <div class="row address-info">
                <div class="col-md-2 col-3 address-left text-lg-center text-sm-right text-center">
                      <i class="fa fa-map"></i>
                    </div>
                    <div class="col-md-10 col-9 address-right">
                      <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
                    </div>
                </div>
              </div>
            </div>
            <!-- social icons footer -->
            <div class="w3l-footer text-center pt-lg-4 mt-5">
              <ul class="list-unstyled">
                <li>
                  <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
                    <img src="image/fb.png" alt="">
                  </a>
                </li>
                <li class="mx-1">
                  <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
                    <img src="image/ig.png" alt="">
                  </a>
                </li>
                <li class="mx-1">
                  <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
                    <img src="image/you.png" alt="">
                  </a>
                </li>
              </ul>
            </div>
            <!-- copyright -->
            <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
            <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
            <!-- //copyright -->
          </div>
        
        
      
    </footer>
    
<script src="assets/jquery-1.12.4-jquery.min.js"></script> 

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<script src="dist/js/bootstrap.min.js"></script>
<script>
      // Create a Stripe client.
      var stripe = Stripe('pk_live_51H8rAqJxEegKNjYzGzdTLdwzbq5UkdUDyxaZxFW81EuD7O4h4X0V8ffx9H3XDQqGnnL9ctYGAoTumgXazWMkq5pI00kra7c8TI');

      // Create an instance of Elements.
      var elements = stripe.elements();

      // Custom styling can be passed to options when creating an Element.
      // (Note that this demo uses a wider set of styles than the guide below.)
      var style = {
        base: {
          color: '#32325d',
          lineHeight: '18px',
          fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };

      // Create an instance of the card Element.
      var card = elements.create('card', {style: style});

      // Add an instance of the card Element into the `card-element` <div>.
      card.mount('#card-element');

      // Handle real-time validation errors from the card Element.
      card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });

      // Handle form submission.
      var form = document.getElementById('payment-form');
      form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(card).then(function(result) {
          if (result.error) {
            // Inform the user if there was an error.
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
          } else {
            // Send the token to your server.
            stripeTokenHandler(result.token);
          }
        });
      });

      function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
      }
    </script>
</body>
</html>