<?php
require_once "CarritoCompras.php";



/* Calculate Cart Total Items */

$item_quantity = 1;
$item_price = 650;


if(!empty($_POST["proceed_payment"])) {
    $name = $_POST ['name'];
    $address = $_POST ['address'];
    $city = $_POST ['city'];
    $zip = $_POST ['zip'];
    $country = $_POST ['country'];
}
$order = 1;

?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Beacon Link</title>
    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/sticky-footer-navbar.css" rel="stylesheet">
    <link href="assets/cart.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
    <!-- Navigation-->
    <header> 
        <!-- Fixed navbar -->
        <nav style="" class="mainNav" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#"> <img src="image/logo.png" class="logo"></a>
                  
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                
                </div>
            </div>
      </nav>
     
    </header> 

<!-- Begin page content -->

<div class="container">
  
  <hr>
  <div class="row">
    <div class="col-12 col-md-12"> 
      <!-- Contenido -->
<div id="shopping-cart">
        <div class="txt-heading">
            <div class="txt-heading-label">Checkout</div>

            <a id="btnEmpty" href="index.php?action=empty"><img
                src="image/empty-cart.png" alt="empty-cart"
                title="Empty Cart" class="float-right" /></a>
            <div class="cart-status">
                <div>Total Quantity: <?php echo $item_quantity ?></div>
                <div>Total Price: $ <?php echo $item_price ?></div>
            </div>
        </div>
    
<?php
            require_once ("ListaCarrito.php");
            ?>


</div>
<?php if(!empty($order)) { ?>
    <form name="frm_customer_detail" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="POST">
    <input type='hidden'
							name='business' value='YOUR_BUSINESS_EMAIL'> <input
							type='hidden' name='item_name' value='Cart Item'> <input
							type='hidden' name='item_number' value="<?php echo $order;?>"> <input
							type='hidden' name='amount' value='<?php echo $item_price; ?>'> <input type='hidden'
							name='currency_code' value='USD'> <input type='hidden'
							name='notify_url'
							value='http://yourdomain.com/shopping-cart-check-out-flow-with-payment-integration/Notificaciones.php'> <input
							type='hidden' name='return'
							value='http://yourdomain.com/shopping-cart-check-out-flow-with-payment-integration/Respuesta.php'> <input type="hidden"
							name="cmd" value="_xclick">  <input
							type="hidden" name="order" value="<?php echo $order;?>">
    <div>
        <input type="submit" class="btn-action"
                name="continue_payment" value="Continuar el Pago">
    </div>
    </form>
<?php } else { ?>
<div class="success">Problema al hacer el pedido. Inténtalo de nuevo!</div>
<div>
        <button class="btn-action">Atras</button>
    </div>
<?php } ?>
      <!-- Fin Contenido --> 
    </div>
  </div>
  <!-- Fin row --> 

  
</div>
<!-- Fin container -->
<footer class="footer py-5">
		
    <div class="container py-xl-4 py-lg-3">
       <div class="address row mb-4">
          <div class="col-lg-4 address-grid">
             <div class="row address-info">
              <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                <i class="fa fa-envelope"></i>
              </div>
              <div class="col-md-9 col-8 address-right">
                <p>
                  <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
                </p>
              </div>

            </div>
          </div>
          <div class="col-lg-4 address-grid my-lg-0 my-4">
            <div class="row address-info">
              <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                <i class="fa fa-phone"></i>
              </div>
              <div class="col-md-9 col-8 address-right">
                <p class="text-light">Language Department<br>
                (470) 315-4949 EXT 301<br>
                Media Department <br>
                (470) 315-4949 EXT 302<br>
                Toll-Free<br>
                (844) 706-7388</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 address-grid">
            <div class="row address-info">
              <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                <i class="fa fa-map"></i>
              </div>
              <div class="col-md-9 col-8 address-right">
                <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
              </div>
            </div>
          </div>
        </div>
        <!-- social icons footer -->
        <div class="w3l-footer text-center pt-lg-4 mt-5">
          <ul class="list-unstyled">
            <li>
              <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
                <img src="image/fb.png" alt="">
              </a>
            </li>
            <li class="mx-1">
              <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
                <img src="image/ig.png" alt="">
              </a>
            </li>
            <li class="mx-1">
              <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
                <img src="image/you.png" alt="">
              </a>
            </li>
          </ul>
        </div>
        <!-- copyright -->
        <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
        <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
        <!-- //copyright -->
      </div>
    
    
  
</footer>

<script src="assets/jquery-1.12.4-jquery.min.js"></script> 

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<script src="dist/js/bootstrap.min.js"></script>
</body>
</html>