$(document).ready(function(){
	$('.sliderHome').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
//	  fade: true,
	  autoplay: true,
	  autoplaySpeed: 4000,
		dots: false,
		adaptiveHeight: true,
	});

	$('.sliderTest').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
//	  fade: true,
	  autoplay: true,
	  autoplaySpeed: 4000,
		dots: false,
		adaptiveHeight: true,
	});

	$(window).scroll(function() {
		if ($(window).scrollTop() > 100) {
			$('header#masthead').addClass("scrolling");
		}else{
			$('header#masthead').removeClass("scrolling");
		}
	});
	$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
	$(".inline").colorbox({inline:true, width:"90%"});
	

	/*function addToCart(p_id) {
            jQuery.ajax({
              type: 'POST',
              url: '/wp/?post_type=product&add-to-cart='+p_id,
              data: { 'product_id':  p_id,
              'quantity': 1},
              success: function(response, textStatus, jqXHR){
                    console.log("Product added");
              }/*,
            });
      }*/

    /*$('#enrollment-training').click(function(e) {
          e.preventDefault();
          addToCart(207);
          return false;
       });    

     function addToCart(p_id) {
          $.post('/wp/?post_type=product&add-to-cart=' + p_id, function() {
             setTimeout(window.location="/checkout", 1000);
          }).done(function() {
		      setTimeout(window.location="/checkout", 1000);
		  })
		  .fail(function() {
		     setTimeout(window.location="/checkout", 1000);
          });

       }*/
     ScrollReveal().reveal('.banner-img', {origin: 'left', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('.style-banner', {origin: 'right', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('.aboutright', {origin: 'bottom', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('.aboutleft', {origin: 'bottom', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('.left-text', {origin: 'left', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('.left-right', {origin: 'right', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('#services .about-in:nth-child(2n)', {origin: 'bottom', distance: "10%", duration: 1000, reset:true, delay: 500});
     ScrollReveal().reveal('#services .about-in:nth-child(2n+1)', {origin: 'top', distance: "10%", duration: 1000, reset:true, delay: 500});
     ScrollReveal().reveal('.team .team-grids:nth-child(1)', {origin: 'bottom', distance: "10%", duration: 1000, delay: 500, reset:true});
     ScrollReveal().reveal('.team .team-grids:nth-child(2)', {origin: 'bottom', distance: "10%", duration: 1000, delay: 700, reset:true});
     ScrollReveal().reveal('.team .team-grids:nth-child(3)', {origin: 'bottom', distance: "10%", duration: 1000, delay: 1000, reset:true});
     ScrollReveal().reveal('.map', {origin: 'left', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('.main_grid_contact', {origin: 'right', distance: "10%", duration: 1000, reset:true});
     ScrollReveal().reveal('#clients .team-grids:nth-child(2n+1)', {origin: 'bottom', distance: "10%", duration: 1000, delay: 500, reset:true});
     ScrollReveal().reveal('#clients .team-grids:nth-child(2n)', {origin: 'bottom', distance: "10%", duration: 1000, delay: 700, reset:true});
     
});
