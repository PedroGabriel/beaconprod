<div class="tab-pane" id="tab6" role="tabpanel" aria-labelledby="six-tab" style="text-align: center;">

  <div>
    <!--Main Content-->
      <div id="form3"  name="form3"  class="wrap">
          <div class="content" id="content-form-usuario">
             <div class="row">
               <div class="col-xs-12 col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="tabpanel">
                                    <!--Opciones del Tab-->
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" style="background-color: white; color: white;">
                                            <a onclick="mensagge()" id="one-tab" class="nav-link" data-toggle="tab" href="#01" role="tab" aria-controls="false">Certified </a>
                                        </li>
                                        <li class="nav-item" style="background-color: white; color: white;">
                                            <a onclick="mensagge()" id="two-tab" class="nav-link" href="#02"  data-toggle="tab" role="tab" aria-controls="false">Trained</a>
                                        </li>
                                        <li class="nav-item" style="background-color: white; color: white;">
                                          <a onclick="mensagge()" id="two-tab" class="nav-link" href="#03"  data-toggle="tab" role="tab" aria-controls="false">Have experience but no training</a>
                                        </li>
                                        <li class="nav-item" style="background-color: white; color: white;">
                                          <a onclick="archivo()" id="two-tab" class="nav-link" href="#04"  data-toggle="tab" role="tab" aria-controls="false">New to the field </a>
                                        </li>
                                        <script>
                                          function archivo()
                                            {
                                            var mensaje;
                                            var opcion = confirm("Attach any Training or Certification Document");
                                            if (opcion == true) {
                                                mensaje = "OK";
                                          } 
                                          document.getElementById("ejemplo").innerHTML = mensaje;
                                          }
                                        function mensagge()
                                            {
                                            var mensaje;
                                            var opcion = confirm("Excellent, Lets complete your aplication!");
                                            if (opcion == true) {
                                                mensaje = "OK";
                                          } 
                                          document.getElementById("ejemplo").innerHTML = mensaje;
                                        }</script> 
                                    </ul>
                                    <br>
                                    <div class="tab-content">
                                      <!--Certified-->
                                      <?php require_once("contact/section6_certified.php") ?>
                                      <!--Trained-->
                                      <?php require_once("contact/section6_trained.php") ?>       
                                      <!--Have Experience-but no training-->
                                      <?php require_once("contact/section6_experience.php") ?>   
                                      <!--Now to the field-->
                                      <?php require_once("contact/section6_new.php") ?>  
                                       
                                      
                                    </div><!--Div Tabcontent-->
                              </div><!--Div Tabpanel-->
                          </div><!--Div Body-->
                      </div><!--Div box-primary-->
                  </div>
                </div>
              </div>
            </div><!--select group-->
        </div><!--div content-form-usuario -->
    </div>   
</div>